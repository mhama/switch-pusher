class PolyBuilder
  def initialize x=nil, y=nil, pt: nil
    @points = []
    @x=@y=0
    @x=x unless x.nil?
    @y=y unless y.nil?
    @x=pt[0] unless pt.nil?
    @y=pt[1] unless pt.nil?
    add_point
  end

  DIRS = {
    r:     [1,0],
    l:     [-1,0],
    u:     [0,1],
    d:     [0,-1],
    right: [1,0],
    left:  [-1,0],
    up:    [0,1],
    down:  [0,-1],
  }

  def self.define_dir_methods
    DIRS.keys.each do |dir|
      define_method(dir) do |length|
        relative dir, length
      end
    end
  end
  define_dir_methods

  def x value
    to value, nil
  end

  def y value
    to nil, value
  end

  def by x, y
    @x += x unless x.nil?
    @y += y unless y.nil?
    add_point
    self
  end

  def to x, y
    @x = x unless x.nil?
    @y = y unless y.nil?
    add_point
    self
  end

  def label name
    @points.last[:labels] ||= []
    @points.last[:labels] << name
    self
  end

  def to_s
    points.to_s
  end

  def points
    @points.map{ |point| point[:pt] }
  end

  def find name
    point = @points.find{ |point| point[:labels] && point[:labels].include?(name) }
    point && point[:pt]
  end

  private
  def add_point
    @points << current_point
  end

  def current_point
    { pt: [@x, @y] }
  end

  def relative dir, length
    vec = DIRS[dir]
    by vec[0]*length, vec[1]*length
  end
end
