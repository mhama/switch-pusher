class LowerStay < CrystalScad::Printed
  attr_accessor :vessel_support_span, :inner_upper, :vessel_support_wall_z_lower_outside, :vessel_support_wall_z_upper_outside, :vessel_end_x, :bottom_y, :top_y

  def initialize switch_frame=nil, upper_stay=nil
    @switch_frame = switch_frame || SwitchFrame.new
    @upper_stay = upper_stay || UpperStay.new(@switch_frame)
    @vessel_margin = [0.4, 0.2, 0.3]
    @inner_upper = (Vector.elements(@upper_stay.under_edge_of_base_plane) + Vector[ -@switch_frame.sw_frame_width - @vessel_margin[0], - @vessel_margin[1]]).to_a
    @inner_wall_thickness = 2.0
    @outer_wall_thickness = 1.0
    @upper_plane_thickness = 1.5
    @upper_plane_width = 30.0
    @bottom_hook_thickness = 1.0
    @bottom_hook_slope_height = 0.5
    @bottom_hook_width = 3.0
    @vessel_margin_z = 0.3
    @vessel_support_span = @upper_stay.vessel_height + @upper_stay.vessel_side_wall_thickness * 2.0 + @vessel_margin[2] * 2.0
    @vessel_support_top_y = @upper_stay.vessel_side_wall_outer_top_point[1]
    @vessel_support_wall_thickness = 2.0
    @vessel_support_wall_z_lower_outside = -@upper_stay.vessel_side_wall_thickness - @vessel_margin[2] - @vessel_support_wall_thickness
    @vessel_support_wall_z_upper_outside = @vessel_support_wall_z_lower_outside + @vessel_support_span + @vessel_support_wall_thickness * 2.0
    @vessel_end_x = @upper_stay.vessel_end_upper_point[0]
    @bolt_diameter = 3.0
    @bolt_hole_point1 = (Vector.elements(@upper_stay.bolt_hole_point_near) + Vector[-6.0,0]).to_a
    @bolt_hole_point2 = (Vector.elements(@upper_stay.bolt_hole_point_near) + Vector[-15.0,0]).to_a
    builder
    vessel_support_wall_builder
  end

  def part(show)
    lower_vessel +
    vessel_support_wall +
    vessel_support_wall.translate(z: @vessel_support_span + @vessel_support_wall_thickness)
  end

  def lower_vessel
    margin = 0.2
    polygon(points: builder.points)
      .linear_extrude(height: @vessel_support_span + margin)
      .translate(z: -@upper_stay.vessel_side_wall_thickness - @vessel_margin[2] - margin*0.5) -
    # 穴あけ
    cube(x:(@inner_upper[0] - @vessel_end_x) * 0.5, y: 10.0, z: @upper_stay.vessel_height * 0.6).translate(x: @vessel_end_x + (@inner_upper[0] - @vessel_end_x) * 0.25, y: 0.0, z: 10.0)
  end

  def builder
    margin = 0.4
    @bottom_y = @switch_frame.outer_bottom_point[1] - margin - @bottom_hook_thickness
    @builder ||= PolyBuilder.new(pt: @inner_upper)
      .y(@bottom_y + @bottom_hook_thickness)
      .by(@bottom_hook_width, -@bottom_hook_slope_height)
      .y(@bottom_y)
      .l(@bottom_hook_width + @inner_wall_thickness)
      .y(@inner_upper[1] - @upper_plane_thickness)
      .x(@vessel_end_x + @outer_wall_thickness)
      .y(@bottom_y)
      .l(@outer_wall_thickness).label(:far_foot_bottom)
      .y(@inner_upper[1]).label(:far_foot_top)
  end

  def vessel_support_wall_builder
    @vessel_base_y = @builder.find(:far_foot_top)[1] + @upper_stay.base_plane_thickness + @vessel_margin[1]
    @top_y = @vessel_base_y + @upper_stay.vessel_depth
    @vessel_support_wall_builder ||= PolyBuilder.new(@inner_upper[0], @vessel_support_top_y)
      .y(@bottom_y)
      .x(@builder.find(:far_foot_bottom)[0])
      .y(@vessel_base_y)
      .by(@upper_stay.vessel_slope_width, @upper_stay.vessel_depth)
  end

  def vessel_support_wall
    (
      polygon(points: vessel_support_wall_builder.points).linear_extrude(height: @vessel_support_wall_thickness) -
      cylinder(d: @bolt_diameter, h: @vessel_support_wall_thickness + 0.2).translate(x: bolt_hole_point1[0], y: bolt_hole_point1[1], z: -0.1) -
      cylinder(d: @bolt_diameter, h: @vessel_support_wall_thickness + 0.2).translate(x: bolt_hole_point2[0], y: bolt_hole_point2[1], z: -0.1)
    )
      .translate(z: @vessel_support_wall_z_lower_outside) #-@upper_stay.vessel_side_wall_thickness - @vessel_margin[2] - @vessel_support_wall_thickness)
  end
end

class LowerCoverWithStay < CrystalScad::Printed

  def initialize lower_stay=nil
    @lower_stay = lower_stay || LowerStay.new
    @base_surface_thickness = 1.5
    @base_surface_width = 65.0
    @base_surface_height = 80.0
    @base_surface_offset_z = 16.0
    @base_surface_x_min = @lower_stay.inner_upper[0] - @base_surface_width
    @wall_thickness = 1.5
  end

  def part(show)
    @lower_stay.part(show) + base_surface + upper_wall + lower_wall
  end

  def base_surface
    polygon(points: builder.points)
      .linear_extrude(height: @base_surface_thickness).rotate(x: 90.0).translate(y: @lower_stay.bottom_y+@base_surface_thickness)
  end

  def builder
    chamfer = 5.0
    # xz plane
    @builder ||= PolyBuilder.new(pt: [@lower_stay.inner_upper[0], @lower_stay.vessel_support_wall_z_upper_outside])
      .u(@base_surface_offset_z - chamfer)
      .by(-chamfer, chamfer)
      .l(@base_surface_width-chamfer*2)
      .by(-chamfer, -chamfer)
      .d(@base_surface_height-chamfer*2)
      .by(chamfer, -chamfer)
      .r(@base_surface_width-chamfer*2)
      .by(chamfer, chamfer)
      .y(@lower_stay.vessel_support_wall_z_lower_outside)
      .x(@lower_stay.vessel_end_x)
      .u(@lower_stay.vessel_support_wall_z_upper_outside - @lower_stay.vessel_support_wall_z_lower_outside)
  end

  def upper_wall stay_on_upside: false
    stay_thickness = 1.5
    stay_width_z = 6.0
    margin = 0.1
    wall_ratio = 0.4
    stay_transform_z = @lower_stay.vessel_support_wall_z_upper_outside + @base_surface_offset_z + (stay_on_upside ? 0 : - @wall_thickness - stay_width_z)
    cube(x:@base_surface_width * wall_ratio, y: @lower_stay.top_y - @lower_stay.bottom_y - @base_surface_thickness + margin, z: @wall_thickness)
      .translate(x: @base_surface_x_min + 10.0, y: @lower_stay.bottom_y + @base_surface_thickness - margin, z: @lower_stay.vessel_support_wall_z_upper_outside + @base_surface_offset_z - @wall_thickness) +
    (cube(x:@base_surface_width * wall_ratio, y: stay_thickness, z: stay_width_z)
      .translate(x: @base_surface_x_min + 10.0, y: @lower_stay.top_y - stay_thickness, z: stay_transform_z) -
    cylinder(d:3.0, h: stay_thickness*2).rotate(x:90.0).translate(x: @base_surface_x_min + 10.0 + 5.0, y: @lower_stay.top_y + (stay_thickness * 0.5), z: stay_transform_z + stay_width_z*0.5) -
    cylinder(d:3.0, h: stay_thickness*2).rotate(x:90.0).translate(x: @base_surface_x_min + 10.0 + @base_surface_width * wall_ratio - 5.0, y: @lower_stay.top_y + (stay_thickness * 0.5), z: stay_transform_z + stay_width_z*0.5)
    )
  end

  def lower_wall
    upper_wall(stay_on_upside: true).translate(z: -@base_surface_height + @wall_thickness)
  end
end
