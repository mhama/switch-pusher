class MeshTag < CrystalScad::Printed
  attr_accessor :size
  def initialize
    @size = [24.0, 48.0, 12.0]
    @header_width = 13.0
    @header_height = 5.0
    @header_offset_z = 2.0
  end

  def part(show)
    body + pin_header
  end

  def body
    cube(x:@size[0], y:@size[1], z:@size[2])
  end

  def pin_header
    cube(x:@header_width, y:10.0, z: @header_height)
      .translate(x: (@size[0]-@header_width) * 0.5, y: -0.3, z: @header_offset_z)
  end

  def pin_position_xz index_x, index_z
    x_offset = (@size[0]-@header_width) * 0.5
    [
      x_offset + (@header_width / 5.0) * index_x + (@header_width / 10.0),
      @header_offset_z + (@header_height / 2.0) * index_z + (@header_height / 4.0)
    ]
  end
end
