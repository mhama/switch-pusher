$fn=64;
union(){difference(){union(){linear_extrude(height = 46.000){polygon(points = [[10.1, 2.1], [8.6, 2.1], [8.6, 0.6], [11.1, 0.6], [11.1, 17.9], [10.1, 17.9], [10.1, 5.9], [-29.9, 5.9], [-29.9, 4.4], [10.1, 4.4]]);
}
translate(v = [0, 0, -2.0])
difference(){linear_extrude(height = 2.000){polygon(points = [[-29.9, 4.4], [-29.9, 5.9], [-22.9, 17.9], [11.1, 17.9], [11.1, 4.4]]);
}
translate(v = [-18.9, 8.9])
union(){cylinder(h = 4.000, r = 1.600);
translate(v = [0, -1.6])
cube(size = [17.0, 3.2, 4.0]);
translate(v = [17.0, 0])
cylinder(h = 4.000, r = 1.600);
}
translate(v = [7.1, 11.9, -1.0])
cylinder(h = 4.000, r = 2.000);
}
translate(v = [0, 0, 48.0])
translate(v = [0, 0, -2.0])
difference(){linear_extrude(height = 2.000){polygon(points = [[-29.9, 4.4], [-29.9, 5.9], [-22.9, 17.9], [11.1, 17.9], [11.1, 4.4]]);
}
translate(v = [-18.9, 8.9])
union(){cylinder(h = 4.000, r = 1.600);
translate(v = [0, -1.6])
cube(size = [17.0, 3.2, 4.0]);
translate(v = [17.0, 0])
cylinder(h = 4.000, r = 1.600);
}
translate(v = [7.1, 11.9, -1.0])
cylinder(h = 4.000, r = 2.000);
}
}
translate(v = [10.0, 6.4, 4.0])
cube(size = [1.2, 11.5, 36.8]);
}
translate(v = [6.1, 5.9, 23.0])
difference(){cube(size = [4.0, 12.0, 3.0]);
translate(v = [2.0, 6.0])
cylinder(h = 3.000, r = 1.000);
}
translate(v = [-21.9, 5.9, 23.0])
difference(){cube(size = [4.0, 12.0, 3.0]);
translate(v = [2.0, 6.0])
cylinder(h = 3.000, r = 1.000);
}
}
