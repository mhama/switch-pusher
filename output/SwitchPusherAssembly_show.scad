$fn=64;
union(){union(){linear_extrude(height = 100, center = true){polygon(points = [[0, 0], [0, 2], [1, 2], [1, 3], [2.3, 3.0], [2.3, 4.3], [10, 4.3], [10, 2.3], [5, 2.3], [3, 0]]);
}
union(){difference(){union(){linear_extrude(height = 46.000){polygon(points = [[10.1, 2.1], [8.6, 2.1], [8.6, 0.6], [11.1, 0.6], [11.1, 17.9], [10.1, 17.9], [10.1, 5.9], [-29.9, 5.9], [-29.9, 4.4], [10.1, 4.4]]);
}
translate(v = [0, 0, -2.0])
difference(){linear_extrude(height = 2.000){polygon(points = [[-29.9, 4.4], [-29.9, 5.9], [-22.9, 17.9], [11.1, 17.9], [11.1, 4.4]]);
}
translate(v = [-18.9, 8.9])
union(){cylinder(h = 4.000, r = 1.600);
translate(v = [0, -1.6])
cube(size = [17.0, 3.2, 4.0]);
translate(v = [17.0, 0])
cylinder(h = 4.000, r = 1.600);
}
translate(v = [7.1, 11.9, -1.0])
cylinder(h = 4.000, r = 2.000);
}
translate(v = [0, 0, 48.0])
translate(v = [0, 0, -2.0])
difference(){linear_extrude(height = 2.000){polygon(points = [[-29.9, 4.4], [-29.9, 5.9], [-22.9, 17.9], [11.1, 17.9], [11.1, 4.4]]);
}
translate(v = [-18.9, 8.9])
union(){cylinder(h = 4.000, r = 1.600);
translate(v = [0, -1.6])
cube(size = [17.0, 3.2, 4.0]);
translate(v = [17.0, 0])
cylinder(h = 4.000, r = 1.600);
}
translate(v = [7.1, 11.9, -1.0])
cylinder(h = 4.000, r = 2.000);
}
}
translate(v = [10.0, 6.4, 4.0])
cube(size = [1.2, 11.5, 36.8]);
}
translate(v = [6.1, 5.9, 23.0])
difference(){cube(size = [4.0, 12.0, 3.0]);
translate(v = [2.0, 6.0])
cylinder(h = 3.000, r = 1.000);
}
translate(v = [-21.9, 5.9, 23.0])
difference(){cube(size = [4.0, 12.0, 3.0]);
translate(v = [2.0, 6.0])
cylinder(h = 3.000, r = 1.000);
}
}
}
union(){union(){union(){difference(){translate(v = [0, 0, -2.4])
linear_extrude(height = 50.800){polygon(points = [[-0.3, 4.2], [-0.3, -0.4], [2.7, -0.9], [2.7, -1.4], [-2.3, -1.4], [-2.3, 2.7], [-28.9, 2.7], [-28.9, -1.4], [-29.9, -1.4], [-29.9, 4.2]]);
}
translate(v = [-22.5, 0.0, 10.0])
cube(size = [14.8, 10.0, 27.6]);
}
translate(v = [0, 0, -4.3])
difference(){linear_extrude(height = 2.000){polygon(points = [[-0.3, 17.9], [-0.3, -1.4], [-29.9, -1.4], [-29.9, 5.9], [-22.9, 17.9]]);
}
translate(v = [-7.9, 8.9, -0.1])
cylinder(h = 2.200, r = 1.500);
translate(v = [-16.9, 8.9, -0.1])
cylinder(h = 2.200, r = 1.500);
}
translate(v = [0, 0, 52.6])
translate(v = [0, 0, -4.3])
difference(){linear_extrude(height = 2.000){polygon(points = [[-0.3, 17.9], [-0.3, -1.4], [-29.9, -1.4], [-29.9, 5.9], [-22.9, 17.9]]);
}
translate(v = [-7.9, 8.9, -0.1])
cylinder(h = 2.200, r = 1.500);
translate(v = [-16.9, 8.9, -0.1])
cylinder(h = 2.200, r = 1.500);
}
translate(v = [0, 0.1])
rotate(a = [90.0, 0, 0])
linear_extrude(height = 1.500){polygon(points = [[-0.3, 50.3], [-0.3, 61.3], [-5.3, 66.3], [-60.3, 66.3], [-65.3, 61.3], [-65.3, -8.7], [-60.3, -13.7], [-5.3, -13.7], [-0.3, -8.7], [-0.3, -4.3], [-29.9, -4.3], [-29.9, 50.3]]);
}
}
union(){translate(v = [-55.3, 0.0, 64.8])
cube(size = [26.0, 17.9, 1.5]);
difference(){translate(v = [-55.3, 16.4, 58.8])
cube(size = [26.0, 1.5, 6.0]);
translate(v = [-50.3, 18.65, 61.8])
rotate(a = [90.0, 0, 0])
cylinder(h = 3.000, r = 1.500);
translate(v = [-34.3, 18.65, 61.8])
rotate(a = [90.0, 0, 0])
cylinder(h = 3.000, r = 1.500);
}
}
}
translate(v = [0, 0, -78.5])
union(){translate(v = [-55.3, 0.0, 64.8])
cube(size = [26.0, 17.9, 1.5]);
difference(){translate(v = [-55.3, 16.4, 66.3])
cube(size = [26.0, 1.5, 6.0]);
translate(v = [-50.3, 18.65, 69.3])
rotate(a = [90.0, 0, 0])
cylinder(h = 3.000, r = 1.500);
translate(v = [-34.3, 18.65, 69.3])
rotate(a = [90.0, 0, 0])
cylinder(h = 3.000, r = 1.500);
}
}
}
translate(v = [-21.9, 5.9, 6.0])
union(){translate(v = [4.25, 0])
cube(size = [23.5, 11.0, 14.0]);
translate(v = [0, 0, 14.0])
cube(size = [32.0, 11.0, 2.0]);
translate(v = [4.25, 0, 16.0])
cube(size = [23.5, 11.0, 4.0]);
translate(v = [22.25, 5.5, 20.0])
cylinder(h = 8.000, r = 5.500);
}
}
