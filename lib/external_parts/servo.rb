class Servo < CrystalScad::Printed
  attr_reader :width, :edge_width, :height, :servo_body_width, :servo_bolt_span
  def initialize 
    @diameter ||= 8.0
    @width ||= 33.0
    @edge_width ||= 5.0
    @height ||= 10.0
    @depth_from_stay ||= 14.0
    @depth_above_stay ||= 4.0
    @stay_thickness ||= 2.0
    @servo_body_width ||= @width - @edge_width * 2.0
    @servo_bolt_span ||= @width - @edge_width
  end

  def part(show)
    cube(x:@width - @edge_width*2.0, y: @height, z:@depth_from_stay).translate(x:@edge_width) +
      cube(x:@width, y: @height, z: @stay_thickness).translate(z:@depth_from_stay) +
      cube(x:@width - @edge_width*2.0, y: @height, z:@depth_above_stay).translate(x:@edge_width, z: @depth_from_stay + @stay_thickness) +
      cylinder(d:@diameter, h:8.0).translate(x: @width-@edge_width-@diameter*0.5, y: @diameter*0.5, z: @depth_from_stay + @stay_thickness + @depth_above_stay)
  end
end
 
class ServoASV15MG < Servo
  def initialize
    @diameter = 11.0
    @width = 32.0
    @edge_width = 4.25
    @height = 11.0
    @depth_from_stay = 14.0
    @depth_above_stay = 4.0
    @servo_bolt_span = 28.0
    super
  end
end

class ServoGwsPico < Servo
  def initialize
    @diameter = 8.0
    @width = 33.0
    @edge_width = 5.0
    @height = 9.0
    @depth_from_stay = 14.0
    @depth_above_stay = 0
    super
  end
end
