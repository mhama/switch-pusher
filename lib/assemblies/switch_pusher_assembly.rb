class SwitchPusherAssembly < CrystalScad::Assembly
		
	# Assemblies are used to show how different parts interact on your design.
 
	# Skip generation of the 'output' method for this assembly.
	# (will still generate 'show')
	skip :output

	def part(show)
    servo = ServoASV15MG.new
	  switch_frame = SwitchFrame.new
    upper_stay = UpperStay.new(switch_frame, servo)

    lower_stay = LowerStay.new(switch_frame, upper_stay)
    lower_cover = LowerCoverWithStay.new(lower_stay)
    servo.translate(x: upper_stay.upper_edge_of_base_plane[0] - servo.width, y: upper_stay.upper_edge_of_base_plane[1], z: 6.0)

    res = switch_frame.show
    res += upper_stay.show
    #res += lower_stay.show
    res += lower_cover.show
    res += servo

		res
	end
end
