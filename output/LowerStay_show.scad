$fn=64;
union(){difference(){translate(v = [0, 0, -2.4])
linear_extrude(height = 50.800){polygon(points = [[-0.3, 4.2], [-0.3, -0.4], [2.7, -0.9], [2.7, -1.4], [-2.3, -1.4], [-2.3, 2.7], [-28.9, 2.7], [-28.9, -1.4], [-29.9, -1.4], [-29.9, 4.2]]);
}
translate(v = [-22.5, 0.0, 10.0])
cube(size = [14.8, 10.0, 27.6]);
}
translate(v = [0, 0, -4.3])
difference(){linear_extrude(height = 2.000){polygon(points = [[-0.3, 17.9], [-0.3, -1.4], [-29.9, -1.4], [-29.9, 5.9], [-22.9, 17.9]]);
}
translate(v = [-7.9, 8.9, -0.1])
cylinder(h = 2.200, r = 1.500);
translate(v = [-16.9, 8.9, -0.1])
cylinder(h = 2.200, r = 1.500);
}
translate(v = [0, 0, 52.6])
translate(v = [0, 0, -4.3])
difference(){linear_extrude(height = 2.000){polygon(points = [[-0.3, 17.9], [-0.3, -1.4], [-29.9, -1.4], [-29.9, 5.9], [-22.9, 17.9]]);
}
translate(v = [-7.9, 8.9, -0.1])
cylinder(h = 2.200, r = 1.500);
translate(v = [-16.9, 8.9, -0.1])
cylinder(h = 2.200, r = 1.500);
}
}
