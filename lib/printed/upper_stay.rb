class UpperStay < CrystalScad::Printed
  attr_reader :bolt_hole_point_far, :bolt_hole_point_near
  attr_reader :vessel_height, :vessel_depth, :under_edge_of_base_plane, :vessel_side_wall_thickness, :vessel_slope_width
  attr_reader :base_plane_thickness, :upper_edge_of_base_place

  def initialize switch_frame=nil, servo=nil
    @switch_frame = switch_frame || SwitchFrame.new
    @servo = servo || Servo.new
    @hook_depth = 1.5
    @hook_height = 1.5
    @base_plane_thickness = 1.5
    @vertical_plane_thickness = 1.0
    @vessel_height = 46.0
    @vessel_width = 40.0
    @vessel_depth = 12.0
    @vessel_side_wall_thickness = 2.0
    @vessel_slope_width = 7.0
    @under_edge_of_base_plane = (Vector.elements(@switch_frame.inner_top_point) + Vector[0.1, 0.1]).to_a
    @upper_edge_of_base_plane = (Vector.elements(@under_edge_of_base_plane) + Vector[0, @base_plane_thickness]).to_a
    @upper_edge_of_hook = (Vector.elements(@switch_frame.inner_bottom_point) + Vector[0.1, -0.2]).to_a
    @bolt_hole_d = 3.2
    @bolt_hole_length = 17.0
    @bolt_hole_point_near =(Vector.elements(@under_edge_of_base_plane) + Vector[-12.0, @base_plane_thickness + 3.0]).to_a
    @bolt_hole_point_far =(Vector.elements(@bolt_hole_point_near) + Vector[- @bolt_hole_length, 0]).to_a
    @servo_holder_width = 4.0
    @servo_holder_thickness = 3.0
    @servo_bolt_d = 2.0
    @servo_width = 33.0
    @servo_holder_z = 23.0
    builder
    vessel_side_polygon_builder
  end

  def part(show)
    polygon(points: builder.points).linear_extrude(height: @vessel_height) +
    vessel_side_wall + 
    vessel_side_wall.translate(z: @vessel_height + @vessel_side_wall_thickness) - vessel_inner_vent +
    servo_holder.translate(x:@upper_edge_of_base_plane[0] - @servo_holder_width, y: @upper_edge_of_base_plane[1], z: @servo_holder_z) + 
    servo_holder.translate(x:@upper_edge_of_base_plane[0] - @servo_holder_width - @servo.servo_bolt_span, y: @upper_edge_of_base_plane[1], z: @servo_holder_z)
  end

  def test
    cylinder(d: @bolt_hole_d, h: @vessel_side_wall_thickness).translate(x: -@bolt_hole_length, y: @bolt_hole_d * 0.5) +
      cube(x: @bolt_hole_length, y: @bolt_hole_d, z: @vessel_side_wall_thickness).translate(x: -@bolt_hole_length) +
    cylinder(d: @bolt_hole_d, h: @vessel_side_wall_thickness).translate(y: @bolt_hole_d * 0.5)
  end

  def vessel_side_polygon_builder
    @vessel_side_polygon_builder ||= PolyBuilder.new(vessel_end_upper_point[0], vessel_end_upper_point[1]-@base_plane_thickness)
      .u(@base_plane_thickness)
      .by(@vessel_slope_width, @vessel_depth).label(:vessel_side_wall_outer_top)
      .x(@under_edge_of_base_plane[0] + @vertical_plane_thickness)
      .d(@vessel_depth + @base_plane_thickness)
  end

  def vessel_side_wall
    (
      polygon(points: vessel_side_polygon_builder.points).linear_extrude(height: @vessel_side_wall_thickness) -
      (
        cylinder(d: @bolt_hole_d, h: @vessel_side_wall_thickness*2) +
        cube(x: @bolt_hole_length, y: @bolt_hole_d, z: @vessel_side_wall_thickness*2).translate(y: -@bolt_hole_d * 0.5) +
        cylinder(d: @bolt_hole_d, h: @vessel_side_wall_thickness*2).translate(x: @bolt_hole_length)
      ).translate(x: @bolt_hole_point_far[0], y: @bolt_hole_point_far[1]) -
      cylinder(d: 4.0, h: @vessel_side_wall_thickness*2)
        .translate(x: @upper_edge_of_base_plane[0] - 3.0, y: @upper_edge_of_base_plane[1] + @vessel_depth * 0.5, z: -@vessel_side_wall_thickness*0.5)
    )
      .translate(z: -@vessel_side_wall_thickness)
  end

  def vessel_side_wall_outer_top_point
    vessel_side_polygon_builder.find :vessel_side_wall_outer_top
  end

  def vessel_inner_vent
    remain_height = 0.5
    margin = 0.2
    cube(x: @vertical_plane_thickness + margin,
         y: @vessel_depth - remain_height,
         z: @vessel_height * 0.8)
      .translate(
    x: @under_edge_of_base_plane[0] - margin * 0.5,
    y: @under_edge_of_base_plane[1] + @base_plane_thickness + remain_height,
    z: 4.0)
  end

  def builder
    @builder ||= PolyBuilder.new(pt: @upper_edge_of_hook).label(:upper_edge_of_hook)
      .l(@hook_depth)
      .d(@hook_height)
      .x(@upper_edge_of_hook[0] + @vertical_plane_thickness)
      .y(@under_edge_of_base_plane[1] + @base_plane_thickness + @vessel_depth)
      .l(@vertical_plane_thickness)
      .d(@vessel_depth)
      .l(@vessel_width).label(:vessel_end_upper)
      .d(@base_plane_thickness)
      .x(@under_edge_of_base_plane[0])
  end

  def vessel_end_upper_point
    @builder.find :vessel_end_upper
  end

  def servo_holder
    cube(x: @servo_holder_width, y: @vessel_depth, z: @servo_holder_thickness) -
      cylinder(d: @servo_bolt_d, h: @servo_holder_thickness).translate(x: @servo_holder_width*0.5, y: @vessel_depth * 0.5)
  end
end
