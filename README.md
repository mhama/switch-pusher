# Switch Pusher 3D Design

.scad designs created with CrystalSCAD (Ruby code)

## environment

Systems that normal ruby/gem runs
Linux, FreeBSD, Mac OS X ...

## prepare

```sh
bundle install
```

## build

```sh
bundle exec ./switch-pusher.rb
```

