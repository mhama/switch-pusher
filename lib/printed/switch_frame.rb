require './polybuilder.rb'
require 'matrix'

class SwitchFrame < CrystalScad::Printed
  attr_accessor :sw_frame_width, :sw_frame_height, :sw_frame_inner_thickness
  def initialize
    @sw_frame_width = 10
    @sw_frame_height = 4.3
    @sw_frame_inner_thickness = 2
  end

  def part(show)
    p builder.points
    polygon(points: builder.points).linear_extrude(height: 100, center: true)
  end

  def builder
    @builder ||= PolyBuilder.new
      .label(:outer_bottom)
      .u(2).label(:outer_top)
      .r(1)
      .u(1)
      .r(1.3)
      .u(1.3)
      .x(@sw_frame_width).label(:inner_top)
      .d(@sw_frame_inner_thickness).label(:inner_bottom)
      .l(5)
      .to(3,0)
  end

  def inner_top_point
    builder.find :inner_top
  end
  def inner_bottom_point
    builder.find :inner_bottom
  end
  def outer_top_point
    builder.find :outer_top
  end
  def outer_bottom_point
    builder.find :outer_bottom
  end
end
