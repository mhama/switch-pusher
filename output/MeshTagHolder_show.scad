$fn=64;
difference(){cube(size = [27.4, 22.0, 15.4]);
translate(v = [1.5, 2.0, 1.5])
cube(size = [24.4, 20.0, 12.4]);
translate(v = [8.5, -0.1, 4.95])
rotate(a = [-90, 0, 0])
cylinder(h = 10.000, r = 0.500);
translate(v = [8.5, -0.1, 7.45])
rotate(a = [-90, 0, 0])
cylinder(h = 10.000, r = 0.500);
translate(v = [11.1, -0.1, 4.95])
rotate(a = [-90, 0, 0])
cylinder(h = 10.000, r = 0.500);
translate(v = [11.1, -0.1, 7.45])
rotate(a = [-90, 0, 0])
cylinder(h = 10.000, r = 0.500);
translate(v = [13.7, -0.1, 4.95])
rotate(a = [-90, 0, 0])
cylinder(h = 10.000, r = 0.500);
translate(v = [13.7, -0.1, 7.45])
rotate(a = [-90, 0, 0])
cylinder(h = 10.000, r = 0.500);
translate(v = [16.3, -0.1, 4.95])
rotate(a = [-90, 0, 0])
cylinder(h = 10.000, r = 0.500);
translate(v = [16.3, -0.1, 7.45])
rotate(a = [-90, 0, 0])
cylinder(h = 10.000, r = 0.500);
translate(v = [18.9, -0.1, 4.95])
rotate(a = [-90, 0, 0])
cylinder(h = 10.000, r = 0.500);
translate(v = [18.9, -0.1, 7.45])
rotate(a = [-90, 0, 0])
cylinder(h = 10.000, r = 0.500);
}
