class MeshTagHolder < CrystalScad::Printed
  def initialize mesh_tag=nil
    @tag = mesh_tag || MeshTag.new
    @holder_depth = 20.0
    @holder_thickness_side = 1.5
    @holder_thickness_bottom = 2.0
    @hold_margin = 0.2
    @pin_hole_diameter = 1.0
  end

  def part(show)
    holder
  end

  def mesh_tag(show)
    @tag.part(show).translate(x: @hold_margin + @holder_thickness_side, y: @holder_thickness_bottom, z: @hold_margin + @holder_thickness_side)
  end

  def holder
    res = cube(x: @tag.size[0] + @hold_margin * 2 + @holder_thickness_side * 2,
         y: @holder_depth + @holder_thickness_bottom,
         z: @tag.size[2] + @hold_margin * 2 + @holder_thickness_side * 2 ) -
    (cube(x: @tag.size[0] + @hold_margin * 2,
         y: @holder_depth,
         z: @tag.size[2] + @hold_margin * 2)
      .translate(x: @holder_thickness_side, y: @holder_thickness_bottom, z: @holder_thickness_side))
    pin_holes.each {|hole| res -= hole}
    res
  end

  def pin_holes
    holes = []
    (0..4).each do |index_x|
      (0..1).each do |index_z|
        pos = @tag.pin_position_xz index_x, index_z
        p "pos: #{pos}"
        holes << cylinder(d: @pin_hole_diameter, h: 10.0).rotate(x:-90).translate(x: pos[0] + @holder_thickness_side + @hold_margin, y:-0.1, z: pos[1] + @holder_thickness_side + @hold_margin)
      end
    end
    holes
  end
end
